
  /* ---------- > Notes database <----------*/

CREATE TABLE IF NOT EXISTS note_table(id_note INTEGER PRIMARY KEY AUTOINCREMENT, noun TEXT, interval TEXT, tone INTEGER, audio_link TEXT);
INSERT INTO note_table(noun, interval, tone, audio_link) VALUES ('DO', 'Tonique', 1, 'assets/son/do1.wav');
INSERT INTO note_table(noun, interval, tone, audio_link) VALUES ('RE', 'Second', 2, 'assets/son/re1.wav');
INSERT INTO note_table(noun, interval, tone, audio_link) VALUES ('MI', 'Third', 3, 'assets/son/mi1.wav');
INSERT INTO note_table(noun, interval, tone, audio_link) VALUES ('FA', 'Fourth', 4,'assets/son/fa1.wav');
INSERT INTO note_table(noun, interval, tone, audio_link) VALUES ('SOL', 'Fifth', 5,'assets/son/sol1.wav');
INSERT INTO note_table(noun, interval, tone, audio_link) VALUES ('LA', 'Sixth', 6,'assets/son/la1.wav');
INSERT INTO note_table(noun, interval, tone, audio_link) VALUES ('SI', 'Seventh', 7, 'assets/son/si1.wav');
INSERT INTO note_table(noun, interval, tone, audio_link) VALUES ('DO', 'Octave', 8, 'assets/son/do2.wav');
