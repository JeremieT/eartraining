import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NativeAudio } from '@ionic-native/native-audio';
import { DatabaseProvider } from './../../providers/database/database';
import { AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-game',
  templateUrl: 'game.html',
})
export class GamePage {
/* ----------> Variable declarations <---------- */
  //General variables
  notes = [];  

  //First note variables
  do_note = [];
  first_note_tone: number;

  //Second note variables
  second_note_noun: string;
  second_note_tone: number;
  second_note_interval: string;

  //Game variables
  random_note : number;
  tone_diff_array = [];
  tone_diff: number;

/* ----------> Constructor <---------- */
  constructor(public navCtrl: NavController, public navParams: NavParams, private nativeAudio: NativeAudio, private databaseprovider: DatabaseProvider, public alertCtrl: AlertController) {
    this.databaseprovider.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        //Load the first Note | Do so far
        this.loadDo();
        //Load all notes from Home for the second note draw
        this.notes = this.navParams.get('notes');
        //console.log("Constructor test"+ '\n'+ "Notes transfert from Home to Game" + '\n', this.notes);
        this.loadToneDiff();     
      }
    })
  }

/* ----------> Loading section <---------- */
  // Load the Do
  loadDo() {
    this.databaseprovider.getDo().then(data => {
      this.do_note = data;
    })
  }
  // Draw random number for the second note
  randomNoteDraw(){
    this.random_note= Math.floor((Math.random() * 7) + 1);
    //return this.random_note;
  } 
  // Load tone differences array
  loadToneDiff(){
    this.databaseprovider.getToneDiff().then(data=>{
      this.tone_diff_array = data;
      console.log("tone_diff_array: ", this.tone_diff_array);
    })
  }   

/* ----------> Play section <---------- */ 
  //Play first note
  playFirstNote(do_note){
    //Play first note
    this.nativeAudio.play(do_note[0].id_note);
    //Get and return the tone   
    this.first_note_tone = do_note[0].tone;
    //return this.first_note_tone;
  }

  //Play second note
  playSecondNote(notes){
    //Draw a random number
    this.randomNoteDraw();
    //Play second note
    this.nativeAudio.play(notes[this.random_note].id_note);
    //Get the corresponding tones
    this.second_note_noun = notes[this.random_note].noun;
    this.second_note_tone = notes[this.random_note].tone;
    this.second_note_interval = notes[this.random_note].interval;
    //return this.second_note_tone;
  }

  //Play first and second note together
  startGameDuo(do_note, notes){
      // Play notes asynchronously 
    this.playFirstNote(do_note);
    this.playSecondNote(notes);
  }
  
  //Play first and second note separately 
  startGameSep(){
      //Play first Note
    this.playFirstNote(this.do_note);
      //Play second Note with delay
    setTimeout(()=>{this.playSecondNote(this.notes)}, 1000);
  }
  
/* ----------> Game section  <---------- */ 
  //Verify player's result
  checkToneDiff(tone_diff_ans){
    console.log("Player answer: "+tone_diff_ans.tone+ '\n'+"Good answer: "+this.second_note_tone)
    if(tone_diff_ans.tone == this.second_note_tone){
      this.goodAnswerAlert();
    }
    else{
      this.wrongAnswerAlert();
    }
  }

  //Tell the player if he's right or wrong
    //Good answer
  goodAnswerAlert(){
      let good_answer = this.alertCtrl.create({
        title: 'Good answer!',
        subTitle: 'Tonic was Do(tone 1) and the second note was '+this.second_note_noun +'(tone '+this.second_note_tone+')',
        buttons: ['OK']
      });
      good_answer.present();
  }
  
    //Wrong answer
  wrongAnswerAlert(){
      let wrong_answer = this.alertCtrl.create({
        title: 'Wrong answer!',
        subTitle: 'Tonic was Do(tone 1) and the second note was '+this.second_note_noun +'(tone '+this.second_note_tone+')',
        buttons: ['OK']
      });
      wrong_answer.present();
  }
}
