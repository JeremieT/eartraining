import { DatabaseProvider } from './../../providers/database/database';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NativeAudio } from '@ionic-native/native-audio';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
/* ----------> Variable declarations <---------- */
  //General variables
    // Note object from array of object "notes"
  note = {};
    //Notes array imported from the database
  notes = [];

/* ----------> Constructor <---------- */
  constructor(public navCtrl: NavController, private databaseprovider: DatabaseProvider, private nativeAudio: NativeAudio) {
    this.databaseprovider.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.loadNotesData();
      };
    })
  }

/* ----------> Notes loading <---------- */
  loadNotesData() {
    //Get notes in array "notes"
    this.databaseprovider.getAllNotes().then(data => {
      this.notes = data;
      console.log("loadNoteData function", this.notes);
      return this.notes;
    }).then(data =>{
      console.log("Data passing", data);
      this.preloadNotesAudio(this.notes);
    })
  }

  //Load notes audio | Ready for the native audio player
  preloadNotesAudio(notes){
    console.log("preloadNotesAudio function");
    console.log("Notes array inserted in the function", notes);
    if (notes.length>0){
      for (var i=0; i< notes.length; i++){
        this.nativeAudio.preloadComplex(notes[i].id_note, notes[i].audio_link, 1, 1, 0);
        console.log("Notes values test", notes[i].id_note, notes[i].audio_link)
      }      
    }  
    else{
      console.log('Error empty notes array');
    }
  }

/* ----------> Play Notes <---------- */
  playNote(note){
    console.log("Start playNote function");
    this.nativeAudio.play(note.id_note);
  }

/* ----------> Navigation <---------- */
goToGame(){
  this.navCtrl.push("GamePage", {
    notes: this.notes
  });
  console.log("Notes array transfered to GamePage"+this.notes);
 }   
 
}
