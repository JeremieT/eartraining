import { SQLite, SQLiteObject } from'@ionic-native/sqlite';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/Rx';
import { Storage } from '@ionic/storage';
import { Platform } from 'ionic-angular';

@Injectable()
export class DatabaseProvider {

/* ----------> Variable declarations <---------- */
  //General variables
  database: SQLiteObject;
  private databaseReady: BehaviorSubject<boolean>;
  randomNumber: number;

/* ----------> Constructor <---------- */
  constructor(private http: Http, public sqlitePorter: SQLitePorter, private storage: Storage, private sqlite: SQLite, private platform: Platform) {
    this.databaseReady = new BehaviorSubject(false);
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'notes.db',
        location: 'default'
      })

      .then((db: SQLiteObject) => {
        this.database = db;
        this.storage.get('database_filled').then(val => {
          if (val) {
            this.databaseReady.next(true);
          } else {
            this.fillDatabase();
          }
        });
      });

    });
  }

 /* ----------> Database filling <---------- */ 
 fillDatabase() {
  this.http.get('assets/data.sql')
    .map(res => res.text())
    .subscribe(sql => {
      this.sqlitePorter.importSqlToDb(this.database, sql)
        .then(data => {
          this.databaseReady.next(true);
          this.storage.set('database_filled', true);
        })
        .catch(e => console.error(e));
    });
  }

 /* ----------> Notes recovery <---------- */
  getAllNotes(): any {
    return this.database.executeSql("SELECT * FROM note_table", []).then(data => {
      let notes = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          notes.push({ 
                          id_note: data.rows.item(i).id_note, 
                          noun: data.rows.item(i).noun,
                          interval : data.rows.item(i).interval, 
                          tone: data.rows.item(i).tone, 
                          audio_link: data.rows.item(i).audio_link,
                    });
        }
      }
      console.log("test notes");
      console.log(notes);
      return notes;
    }, err => {
      console.log('Error: ', err);
      return [];
    });
  }

 /* ----------> Game Section <---------- */

    /* ----------> Get first note frome database, Do in this case <---------- */
  getDo(): any {
    return this.database.executeSql("SELECT * FROM note_table where tone = " +1, []).then((data) => {
      let do_note = [];
      do_note.push({
        id_note: data.rows.item(0).id_note,
        noun: data.rows.item(0).noun,
        tone: data.rows.item(0).tone,
        audio_link: data.rows.item(0).audio_link,
      });
      return do_note;     
    }, err => {
      console.log('Error: ', err);
      return [];
    });
  }

  /* ----------> Get note nouns, tone differences and interval(from Do) from database <---------- */  
  getToneDiff(): any {
    return this.database.executeSql("SELECT * FROM note_table", []).then(data =>{
      let tone_diff_array = [];
      if(data.rows.length > 0){
        for(var i=1; i<data.rows.length; i++){
          tone_diff_array.push({
            noun : data.rows.item(i).noun,
            interval: data.rows.item(i).interval,
            tone: data.rows.item(i).tone
          });
        }
      }
      console.log("getToneDiff function test : ", tone_diff_array);
      return tone_diff_array;
    }, err => {
      console.log('Error: ', err);
      return [];
    });
  }
  /* ----------> Database readiness verification <---------- */
  getDatabaseState() {
    return this.databaseReady.asObservable();
  }
 
}
